# MATE

The MATE desktop environment for Slackware Linux

## NOTES:

For epub support in atril, webkit2gtk is a required build dependency.

# 

Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)
